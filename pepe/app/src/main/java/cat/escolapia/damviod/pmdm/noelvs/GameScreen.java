package cat.escolapia.damviod.pmdm.noelvs;
import java.util.*;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Handler;

import android.graphics.Color;
import android.os.CountDownTimer;

import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Input.TouchEvent;
import cat.escolapia.damviod.pmdm.framework.Pixmap;
import cat.escolapia.damviod.pmdm.framework.Screen;

/**
 * Created by Martín on 15/01/2017.
 */

public class GameScreen extends Screen {

    static int notready=4;
    enum GameState {
        Ready,
        Running,
        Paused,
        GameOver
    }

    GameState state = GameState.Ready;
  World world;
    int count=60;
    int kount=100;
    int x;
    int y;
    Noel fields[];
    int hola;
    King rei[];
    int xf;
    int yf;
    int xi;
    int yi;
    int distx;
    int disty;
   // int s;
  //  int z;
    boolean win=false;
    static int oldScore = 0;
    //String score = "0";
    public GameScreen(Game game) {
        super(game);
    world = new World();
    }
    public void update(float deltaTime) {
        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();


        if (state == GameState.Ready)
            updateReady(touchEvents);
        if (state == GameState.Running)
            updateRunning(touchEvents, deltaTime);
       if (state == GameState.Paused)
            updatePaused(touchEvents);
        if(state == GameState.GameOver)
            updateFinish(touchEvents);
       /* if (state == GameState.GameOver)
            updateGameOver(touchEvents);*/
    }




    private void updateReady(List<TouchEvent> touchEvents) {
        --kount;
        if(kount<=0){
            state = GameState.Running;
        }

    }
    private void updateRunning(List<TouchEvent> touchEvents, float deltaTime) {
        world.update();
        if(world.gameover())state = GameState.GameOver;

        int len = touchEvents.size();
        int soyo=world.imalive();
        world.choca();
        for (int i = 0; i < len; i++) {

            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if(event.x < 64 && event.y < 64) {
                    Assets.click.play(1);
                    state = GameState.Paused;
                    return;
                }
            }
            if (event.type == TouchEvent.TOUCH_DOWN) {
                getTouchInitial(event);
            }

            if (event.type == TouchEvent.TOUCH_UP) {
                getTouchFinal(event);

                //Slide
             //   System.out.println("esto es el rey" + soyo);

                if ( Math.abs(distx) >20) {
                    if (distx < -30) {

                        world.kingus[soyo].setDirection(King.LEFT);
                        world.kingus[soyo].advance();

                    } else if (distx > 30) {
                        world.kingus[soyo].setDirection(King.RIGHT);
                        world.kingus[soyo].advance();
                      //  System.out.println("esto es verdad"+world.choca());
                    }
                    }
                else if (disty != 0) {
                    if (disty < -30) {
                        world.kingus[soyo].setDirection(King.UP);
                        world.kingus[soyo].advance();
                       // System.out.println("esto es verdad"+world.choca());
                    }

                }

                distx=0;
                disty=0;
            }

            if(world.kingus[soyo].y<0)world.Arriba();



        }
        /*for(int r = 0;r <13;++r)
        {

            s= fields[r].x * 32;
            z = fields[r].y * 32;

        }

        x = rei[hola].x*32;
        y = rei[hola].y*32;
        */
        //System.out.println("esto es la x:"+x+"esto es la y:"+y);
    }
    private void updatePaused(List<TouchEvent> touchEvents) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if(event.x > 80 && event.x <= 240) {
                    if(event.y > 100 && event.y <= 148) {
                        Assets.click.play(1);
                        state = GameState.Running;
                        return;
                    }
                    if(event.y > 180 && event.y < 230) {
                        Assets.click.play(1);
                        game.setScreen(new MainMenuScreen(game));
                        return;
                    }
                }
            }
        }
    }

    private void updateFinish(List<TouchEvent> touchEvents)
    {
        --count;
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_DOWN &&count<=0) {


                    game.setScreen(new MainMenuScreen(game));
                }




                    }


    }

    private void getTouchInitial(TouchEvent event){

     xi = event.x;
    yi = event.y;
      //
    }

    private void getTouchFinal(TouchEvent event){
       xf = event.x;
         yf = event.y;
    //    System.out.println("esto es la segunda x" + xf);
        distx = xf-xi;
      disty = yf-yi;
    //    System.out.println("esto es la diferencia" + distx);
    }
    @Override
    public void render(float deltaTime) {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.gamesc, 0, 0);

        drawWorld(world);

        if(state == GameState.Ready)
            drawReadyUI();
        if(state == GameState.Running)
            drawRunningUI();
        if(state == GameState.Paused)
            drawPausedUI();
        if(state == GameState.GameOver)
            drawGameOver();
    }
private void drawWorld(World world)
{
    Graphics g = game.getGraphics();
    fields = world.fields;
   rei =world.kingus;
     hola=world.imalive();



    Pixmap rey= Assets.king;

    Pixmap Kings = Assets.noel;

    if(hola==0)rey= Assets.king;

    else if(hola==1)rey= Assets.redking;
    else if(hola==2)rey= Assets.whiteking;
    System.out.println(hola);
    for(int i = 0;i <13;++i)
    {
        g.drawPixmap(Kings, world.fields[i].x*32, world.fields[i].y*32);
    }
    g.drawPixmap(rey, world.kingus[hola].x*32,world.kingus[hola].y*32);


}

        public void  drawReadyUI()
        {
            Graphics g = game.getGraphics();

            if(kount>66 &&kount<100 )g.drawPixmap(Assets.tress, 47, 100);
            else if(kount< 66&&kount> 33)g.drawPixmap(Assets.doss, 47, 100);
            else  if(kount< 33&&kount> 0)g.drawPixmap(Assets.unoo, 47, 100);
            else if(kount< 30&&kount< 20)g.drawPixmap(Assets.sero, 47, 100);
            //g.drawLine(0, 416, 480, 416, Color.WHITE);
        }
    private void drawPausedUI() {
        Graphics g = game.getGraphics();

        g.drawPixmap(Assets.pause, 80, 100);
        g.drawPixmap(Assets.exit, 80, 170);
        g.drawLine(0, 416, 480, 416, Color.RED);
    }
    private void drawRunningUI() {
      Graphics g = game.getGraphics();
        g.drawPixmap(Assets.buttons, 0, 0, 64, 128, 64, 64);

      g.drawLine(0, 416, 480, 416, Color.WHITE);


    }
    private void drawGameOver() {
        Graphics g = game.getGraphics();
    if(world.Result()>0)g.drawPixmap(Assets.gana,0,0);
    else g.drawPixmap(Assets.gameover,0,0);

    }




    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
