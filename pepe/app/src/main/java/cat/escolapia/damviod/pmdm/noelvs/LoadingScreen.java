package cat.escolapia.damviod.pmdm.noelvs;

/**
 * Created by martin.sanchez on 11/01/2017.
 */
import cat.escolapia.damviod.pmdm.framework.Game;
import cat.escolapia.damviod.pmdm.framework.Graphics;
import cat.escolapia.damviod.pmdm.framework.Screen;
import cat.escolapia.damviod.pmdm.framework.Graphics.PixmapFormat;
public class LoadingScreen extends Screen{
    public LoadingScreen(Game game) { super(game);}

    @Override
    public void update(float deltaTime) {
        Graphics g = game.getGraphics();
        Assets.redking = g.newPixmap("redking.jpg", PixmapFormat.RGB565);
        Assets.background = g.newPixmap("background.png", PixmapFormat.RGB565);
    Assets.logo= g.newPixmap("logon.png", PixmapFormat.RGB565);
        Assets.gana= g.newPixmap("win.jpg", PixmapFormat.RGB565);
        Assets.gameover= g.newPixmap("gameober.jpg", PixmapFormat.RGB565);
       Assets.play = g.newPixmap("playb.png", PixmapFormat.RGB565);
        Assets.buttons = g.newPixmap("buttons.png", PixmapFormat.ARGB4444);
        Assets.credit = g.newPixmap("credits.png", PixmapFormat.RGB565);
        Assets.gamesc = g.newPixmap("gamescreen.png", PixmapFormat.RGB565);
        Assets.tress=g.newPixmap("tres.png", PixmapFormat.RGB565);
        Assets.doss=g.newPixmap("dos.png", PixmapFormat.RGB565);
        Assets.unoo=g.newPixmap("uno.png", PixmapFormat.RGB565);
        Assets.sero=g.newPixmap("zero.png", PixmapFormat.RGB565);
        Assets.pause = g.newPixmap("pausemenu.png",PixmapFormat.RGB565);
        Assets.noel = g.newPixmap("papanoel.png",PixmapFormat.RGB565);
        Assets.whiteking= g.newPixmap("whitemage.png", PixmapFormat.RGB565);
        Assets.king = g.newPixmap("blacking.jpg", PixmapFormat.RGB565);
        Assets.click = game.getAudio().newSound("click.ogg");
        Assets.exit = g.newPixmap("Quit.png", PixmapFormat.RGB565);
        Assets.nigga= g.newPixmap("black.png", PixmapFormat.RGB565);
    game.setScreen(new MainMenuScreen(game));
    }

    @Override
    public void render(float deltaTime) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }

}
