package cat.escolapia.damviod.pmdm.noelvs;

/**
 * Created by Martín on 15/01/2017.
 */
import java.util.Random;
public class World {
    static final int WORLD_WIDTH = 10;
    static final int WORLD_HEIGHT = 13;

    public Noel noel;

    Random r = new Random();

   Noel fields[] = new Noel[WORLD_HEIGHT];
  //King king = new King();


   King kingus[] = new King[3];


    public World(){

        placeNoels();
        for(int i = 0;i < kingus.length;i++)
        {
            kingus[i]=new King();
        }
    }
    private void placeNoels()
    {
        for (int x = 0; x < 1; x++) {
            for (int y = 0; y < WORLD_HEIGHT; y++) {
                fields[y]= new Noel(x,y);
            }
        }

    }
    public void update()
    {
        int x = r.nextInt(13);
        if(x%2==0)fields[x].MoveDreta();
        else fields[x].MoveEsquerra();
        ++x;
    }
    public boolean choca()
    {
        int s = imalive();
     for(int i = 0;i < WORLD_HEIGHT;++i)
     {
     if(fields[i].x==kingus[s].x && fields[i].y==kingus[s].y)
     {
         kingus[s].Alive=false;
         return true;
     }
     }
        return false;
    }
    public int imalive()
    {
        for(int i = 0;i < kingus.length;++i)
        {
            if(kingus[i].Alive)return i;
        }
        return 1;
    }
    public boolean gameover()
    {
        for(int i = 0;i < kingus.length;++i)
        {
            if(kingus[i].Alive)return false;
        }
        return true;
    }

    public int Result()
    {
        int eso=0;
    for(int i = 0;i < kingus.length;++i)if(kingus[i].win==true)++eso;
        return eso;
    }
    public void Arriba()
    {
        int s = imalive();
        kingus[s].Alive=false;
        kingus[s].win=true;
    }

}

